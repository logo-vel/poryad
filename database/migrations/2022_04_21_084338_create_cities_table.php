<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();

            $table->foreignId('community_id')
                ->after('id')
                ->references('id')
                ->on('communities');
            $table->foreignId('region_id')
                ->after('id')
                ->references('id')
                ->on('regions');

            $table->string('category');
            $table->string('code');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreignId('city_id')
                ->after('id')
                ->nullable()
                ->references('id')
                ->onDelete('set null')
                ->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
};

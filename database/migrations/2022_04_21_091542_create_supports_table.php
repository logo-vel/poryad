<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supports', function (Blueprint $table) {
            $table->id();
            $table->foreignId('status_id')
                ->references('id')
                ->on('support_statuses');
            $table->foreignId('city_id')
                ->references('id')
                ->on('cities');
            $table->foreignId('category_id')
                ->references('id')
                ->on('support_categories');
            $table->foreignId('needy_user_id')
                ->nullable()
                ->references('id')
                ->on('users');
            $table->foreignId('assist_user_id')
                ->nullable()
                ->references('id')
                ->on('users');
            $table->boolean('status');

            $table->string('title');
            $table->mediumText('description');
            $table->string('degree');

            $table->boolean('delivery')->default(0);
            $table->string('delivery_type')->nullable();
            $table->string('delivery_address')->nullable();

            $table->boolean('is_other_needy')->default(0);

            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->string('phone')->nullable();
            $table->string('telegram')->nullable();
            $table->string('viber')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supports');
    }
};

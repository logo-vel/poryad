<?php

namespace Database\Seeders;

use App\Models\SupportStatus;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SupportStatusSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SupportStatus::create([
            'title' => 'На розгляді',
            'color' => '#4F9AD7',
            'is_default' => false,
            'is_notify' => true
        ]);

        SupportStatus::create([
            'title' => 'В пошуці',
            'color' => '#4F9AD7',
            'is_default' => true,
            'is_notify' => false
        ]);

        SupportStatus::create([
            'title' => 'Допомога знайдена',
            'color' => '#50bf44',
            'is_default' => false,
            'is_notify' => false
        ]);
    }
}

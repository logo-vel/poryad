<?php

namespace Database\Seeders;

use App\Models\SupportCategory;
use Illuminate\Database\Seeder;

class SupportCategorySeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $res = [
            [
                'title' => 'транспорт',
            ],
            [
                'title' => 'iжа',
            ],
            [
                'title' => 'одяг',
            ],
            [
                'title' => 'лiки',
            ],
            [
                'title' => 'житло',
            ],
            [
                'title' => 'iнше',
            ]
        ];
        foreach ($res as $r){
            SupportCategory::create($r);
        }
    }
}

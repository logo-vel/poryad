<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Community;
use App\Models\Region;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class LocationSeed extends Seeder
{
    /*
      "object_category" => "СМТ"
      "object_name" => "АЕРОФЛОТСЬКИЙ"
      "object_code" => 110165300
      "region" => "АВТОНОМНА РЕСПУБЛІКА КРИМ"
      "community" => "СІМФЕРОПОЛЬСЬКИЙ РАЙОН"
     */

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = Storage::disk('storage')->get('CitiesAndVillages - 14 March.json');
        $cities = collect(json_decode($file, true));
        $cities->each([$this, 'mapCity'])->toArray();
    }

    public function firstOrCreate($model, $object)
    {
        return $model::firstOrCreate([
            'name' => $object
        ]);
    }

    public function mapCity($city)
    {
        $region = $this->firstOrCreate(Region::class, Str::lower($city['region']));
        $community = $this->firstOrCreate(Community::class, Str::lower($city['community']));
        City::create([
            'region_id' => $region->id,
            'community_id' => $community->id,
            'code' => (int) Str::lower($city['object_code']),
            'category' => Str::slug(Str::lower($city['object_category'])),
            'name' => Str::lower($city['object_name']),
        ]);
    }
}

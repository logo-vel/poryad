export function isEqualPhones(phone1, phone2) {
    phone1 = normalizePhone(phone1)
    phone2 = normalizePhone(phone2)

    return phone1 == phone2
}

function normalizePhone(str) {
    str = str ? str : ''
    str = str.replace(/\+/, '');
    str = str.replace(/\-/, '');
    str = str.replace(/\(/, '');
    str = str.replace(/\)/, '');
    str = str.replace(/\-/, '');
    str = str.replace(/380/, '');

    return str
}

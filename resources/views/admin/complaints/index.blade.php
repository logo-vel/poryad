@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Complaints</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route("admin.index") }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Complaints</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example2" class="table table-bordered table-hover dataTable dtr-inline" aria-describedby="example2_info">
                                            <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>text</th>
                                                <th>support</th>
                                                <th>support id</th>
                                                <th>status</th>
                                                <th>created_at</th>
                                                <th>
                                                    action
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($items as $item)
                                                <tr class="@if($loop->iteration % 2 == 0)odd @else even @endif">
                                                    <td>{{ $item->id }}</td>
                                                    <td>{{ $item->text }}</td>
                                                    <td>
                                                        @if($item->support)
                                                            <a href="{{ route('admin.supports.show', ['support' => $item->support]) }}">{{ $item->support->title }}</a>
                                                        @else
                                                            no set
                                                        @endif
                                                    </td>
                                                    <td>{{ $item->support_id }}</td>
                                                    <td>
                                                        @if($item->status == $complaint_status_open)
                                                            open status
                                                        @elseif($item->status == $complaint_status_close)
                                                            close status
                                                        @endif
                                                    </td>
                                                    <td>{{ date_time_from_db($item->created_at) }}</td>
                                                    <td class="project-actions">
                                                        @if($item->status == $complaint_status_open)
                                                            <a class="btn btn-success btn-sm" href="{{ route('admin.complaints.change-status', ['complaint' => $item, 'status' => $complaint_status_close]) }}">
                                                                set close status
                                                            </a>
                                                        @elseif($item->status == $complaint_status_close)
                                                            <a class="btn btn-info btn-sm" href="{{ route('admin.complaints.change-status', ['complaint' => $item, 'status' => $complaint_status_open]) }}">
                                                                set open status
                                                            </a>
                                                        @endif
                                                        <form class="d-inline-block" method="POST" action="{{ route('admin.complaints.delete', ['complaint' => $item]) }}">
                                                            @csrf
                                                            @method("DELETE")
                                                            <button class="btn btn-danger btn-sm" type="submit">
                                                                delete
                                                            </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        {{ $items->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

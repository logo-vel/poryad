@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Supports</h1>
                    <p>Закрытых запросов: {{ $listing->count() }}</p>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route("admin.index") }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Supports</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example2" class="table table-bordered table-hover dataTable dtr-inline" aria-describedby="example2_info">
                                            <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>title</th>
                                                <th>description</th>
                                                <th>city</th>
                                                <th>category</th>
                                                <th>user</th>
                                                <th>user phone</th>
                                                <th>status</th>
                                                <th>Критичність</th>
                                                <th>created_at</th>
                                                <th>
                                                    Action
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($items as $item)
                                                    <tr class="@if($loop->iteration % 2 == 0)odd @else even @endif">
                                                        <td>{{ $item->id }}</td>
                                                        <td>{{ $item->title }}</td>
                                                        <td>{{ mb_substr($item->description, 0, 100) }}</td>
                                                        <td>{{ $item->relation_city->name }}</td>
                                                        <td>{{ $item->relation_category->title }}</td>
                                                        <td>
                                                            @if($item->user)
                                                                <a href="{{ route('admin.users.edit', ['user' => $item->user]) }}">{{ $item->user->name }}</a>
                                                            @endif
                                                        </td>
                                                        <td>{{ $item->user->phone }}</td>
                                                        <td>
{{--                                                            {{ $item->supportStatus->title }}--}}
                                                            @if($item->status === true)
                                                                Включен
                                                            @elseif($item->status === false)
                                                                Отключен
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if($item->degree == 'grateful')
                                                                Буду вдячний
                                                            @elseif($item->degree == 'wait')
                                                                Терміново, але готовий зачекати
                                                            @elseif($item->degree == 'urgently')
                                                                Терміново потрібно
                                                            @endif
                                                        </td>
                                                        <td>{{ date_time_from_db($item->created_at) }}</td>
                                                        <td class="project-actions">
                                                            <a class="btn btn-info btn-sm" href="{{ route('admin.supports.show', ['support' => $item]) }}">
                                                                show
                                                            </a>
                                                            @if($item->status === true)
                                                                <a class="btn btn-success btn-sm" href="{{ route('admin.supports.change-status', ['support' => $item, 'status' => false]) }}">
                                                                    Выключить
                                                                </a>
                                                            @elseif($item->status === false)
                                                                <a class="btn btn-info btn-sm" href="{{ route('admin.supports.change-status', ['support' => $item, 'status' => true]) }}">
                                                                    Включить
                                                                </a>
                                                            @endif
                                                            <form class="d-inline-block" method="POST" action="{{ route('admin.supports.delete', ['support' => $item]) }}">
                                                                @csrf
                                                                @method("DELETE")
                                                                <button class="btn btn-danger btn-sm" type="submit">
                                                                    delete
                                                                </button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        {{ $items->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Supports</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route("admin.index") }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{ route("admin.supports.index") }}">Supports</a></li>
                        <li class="breadcrumb-item active">Support - {{ $support->id }}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Ім’я та прізвище</label>
                                        <input value="{{ $support->user->name }}" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Телефон</label>
                                        <input value="{{ $support->user->phone }}" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input value="{{ $support->user->email }}" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Заголовок</label>
                                        <input value="{{ $support->title }}" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Опис</label>
                                        <input value="{{ $support->description }}" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Місто <div class="quote"><div class="quote__text">Окрім тимчасово окупованих територій</div></div></label>
                                        <input value="{{ $support->relation_city->name }}" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Категорію</label>
                                        <input value="{{ $support->relation_category->title }}" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Критичність</label>
                                        @if($support->degree == 'grateful')
                                            <input value="Буду вдячний" type="text" class="form-control" disabled>
                                        @elseif($support->degree == 'wait')
                                            <input value="Терміново, але готовий зачекати" type="text" class="form-control" disabled>
                                        @elseif($support->degree == 'urgently')
                                            <input value="Терміново потрібно" type="text" class="form-control" disabled>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Телеграм</label>
                                        <input value="{{ $support->telegram }}" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Вайбер</label>
                                        <input value="{{ $support->viber }}" type="text" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label>Вайбер відповідає номеру</label>
                                        @if($support->viber == $support->user->phone)
                                            <input value="Yes" type="text" class="form-control" disabled>
                                        @else
                                            <input value="No" type="text" class="form-control" disabled>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @if($support->delivery)
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Доставка</label>
                                            <input value="Потрібно доставити" type="text" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Служба доставки</label>
                                            @if($support->delivery_type == 'np')
                                                <input value="Нова пошта" type="text" class="form-control" disabled>
                                            @elseif($support->delivery_type == 'up')
                                                <input value="Укр. пошта" type="text" class="form-control" disabled>
                                            @elseif($support->delivery_type == 'other')
                                                <input value="Інше" type="text" class="form-control" disabled>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            @if($support->delivery_address == 'np')
                                                <label>Місто та відділення вашої пошти</label>
                                                <input value="{{ $support->delivery_address }}" type="text" class="form-control" disabled>
                                            @elseif($support->delivery_type == 'up')
                                                <label>Місто та відділення вашої пошти</label>
                                                <input value="{{ $support->delivery_address }}" type="text" class="form-control" disabled>
                                            @elseif($support->delivery_type == 'other')
                                                <label>Бажаний спосіб доставки</label>
                                                <input value="{{ $support->delivery_address }}" type="text" class="form-control" disabled>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Доставка</label>
                                            <input value="Є можливість забрати" type="text" class="form-control" disabled>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

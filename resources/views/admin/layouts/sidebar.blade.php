<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../index3.html" class="brand-link">
        <img src="/images/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminPanel</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">




        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('admin.index') }}" class="nav-link @if(request()->is('admin')) active @endif">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.users.index') }}" class="nav-link @if(request()->is('admin/users*')) active @endif">
                        <i class="nav-icon fas fa-user"></i>
                        <p>Users</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.supports.index') }}" class="nav-link @if(request()->is('admin/supports*')) active @endif">
                        <i class="nav-icon fas fa-th"></i>
                        <p>Supports</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin.complaints.index') }}" class="nav-link @if(request()->is('admin/complaints*')) active @endif">
                        <i class="nav-icon fas fa-exclamation-triangle"></i>
                        <p>Complaints</p>
                    </a>
                </li>
                <li class="nav-item">
                    <form action="{{ route("admin.logout") }}" method="POST">
                        @csrf
                        <button type="submit" class="nav-link @if(request()->is('admin/supports*')) active @endif">
                            <p>Logout</p>
                        </button>
                    </form>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
<script>
    import Button from "@/Components/Button";
    export default {
        components: {Button}
    }
</script>

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel';

window.$ = window.jQuery = $;



$(function ()  {


    if(window.location.hash === '#donate_crypto') {

        $('.donatCtrl[href="#don_3"]').addClass('active');
        $('.donatCtrl[href="#don_3"]').closest('li').siblings().find('a').removeClass('active');

        $('#don_3').addClass('active');
        $('#don_3').siblings().removeClass('active');

        var
            img =   $('.donatCtrl[href="#don_3"]').data().img;

        $('#'+img).addClass('active');
        $('#'+img).siblings().removeClass('active');
    }

    var
        btn = $('.main__bittons');

    if(btn.length > 0) {
        var
            btnTop = btn.offset().top,
            btnH = btn.height(),
            sT = btnTop + btnH;

        console.log(sT);

        $(window).scroll(function () {
            var
                sctollT = $(window).scrollTop();

            if(sctollT > sT) {
                $('.header__fixed').addClass('showed');
            } else {
                $('.header__fixed').removeClass('showed');
            }
        });
    }


    $('.header__current').click(function (e) {
       e.preventDefault();
    });

    carousell();




    $(document).on('focusin', '.formRequest input', function () {
        $('.selector__ctr').removeClass('opened');
        $('.selector__list').slideUp(300);
    });

    $(document).on('change', '.formRequest input', function () {
        $('.selector__ctr').removeClass('opened');
        $('.selector__list').slideUp(300);
    });


    $(document).on('focusin', '.select__city input', function () {
        var
            $this = $(this),
            list = $this.closest('.select__city').find('.select_citylist');

        $this.addClass('focused');
        list.slideDown(300);
        list.addClass('openedCity');
    });


    $(document).on('focusout', '.select__city input', function () {
        var
            $this = $(this),
            list = $this.closest('.select__city').find('.select_citylist');

        setTimeout(function () {
            $this.removeClass('focused');
            list.slideUp(300);
            list.addClass('openedCity');
        },150);

    });


    $(document).on('click', '.ankor', function (e) {
            e.preventDefault();
        var
            thisAnk = $(this).attr('href'),
            hH = $('.header__fixed').height(),
            $target = $(thisAnk),
            offset = $target.offset().top - hH;



        $('html, body').stop().animate({'scrollTop': offset}, 1000, 'swing', function () {

        });
    });


    // animation();
    startAnim();

});


window.startAnim = function() {
    $('.startAnim').each(function () {
        var
            $this = $(this),
            startDel = $this.data().delay;

        setTimeout(function () {
            $this.addClass('startDone');
        },startDel);
    });
}


//Price carousell
window.carousell = function() {

    if($('.owl-carousel').length > 0) {
        if ( $(window).width() < 1025 ) {
            worksCarousell();
            partCarousell();
            foundersCarousell();
        } else {
            $('.works__carousel').addClass('disabledCar');
            $('.partnears__carousel').addClass('disabledCar');
            $('.founders__carousel').addClass('disabledCar');
        }

        $(window).resize(function() {
            if ( $(window).width() < 1025 ) {
                worksCarousell();
                partCarousell();
                foundersCarousell();
                $('.works__carousel').removeClass('disabledCar');
                $('.partnears__carousel').removeClass('disabledCar');
                $('.founders__carousel').removeClass('disabledCar');
            } else {
                stopWorksCarousel();
                stopPartCarousel();
                stopFoundersCarousel();
                $('.works__carousel').addClass('disabledCar');
                $('.partnears__carousel').addClass('disabledCar');
                $('.founders__carousel').addClass('disabledCar');
            }
        });
    }

    newsCarousell();
}



window.worksCarousell = function() {
    var owl = $(".works__carousel").owlCarousel({
        smartSpeed: 1000,
        mouseDrag: true,
        autoplay: false,
        center: false,
        addClassActive : true,
        loop: true,
        dots: false,
        nav: true,
        dotsEach: 1,
        touchDrag: true,
        dotsSpeed: 1000,
        navSpeed: 1000,
        stagePadding: 0,
        margin: 24,
        responsive : {
            0 : {
                items: 1,
            },
            560 : {
                items: 2,
            },
            767 : {
                items: 3,
            }
        }
    });
}

window.stopWorksCarousel = function() {
    var owl = $('.works__carousel');
    owl.trigger('destroy.owl.carousel');
    owl.addClass('disabledCar');
}



//Founders carousell
window.foundersCarousell = function() {
    var owl = $(".founders__carousel").owlCarousel({
        smartSpeed: 1000,
        mouseDrag: true,
        autoplay: false,
        center: false,
        addClassActive : true,
        loop: true,
        dots: false,
        nav: true,
        dotsEach: 1,
        touchDrag: true,
        dotsSpeed: 1000,
        navSpeed: 1000,
        stagePadding: 0,
        margin: 24,
        responsive : {
            0 : {
                items: 1,
            },
            560 : {
                items: 2,
            },
            767 : {
                items: 3,
            }
        }
    });
};

window.stopFoundersCarousel = function() {
    var owl = $('.founders__carousel');
    owl.trigger('destroy.owl.carousel');
    owl.addClass('disabledCar');
}



//Partners carousell
window.partCarousell = function() {
    var owl = $(".partnears__carousel").owlCarousel({
        smartSpeed: 1000,
        mouseDrag: true,
        autoplay: false,
        center: false,
        addClassActive : true,
        loop: true,
        dots: false,
        nav: true,
        dotsEach: 1,
        touchDrag: true,
        dotsSpeed: 1000,
        navSpeed: 1000,
        stagePadding: 0,
        margin: 24,
        responsive : {
            0 : {
                items: 1,
            },
            560 : {
                items: 2,
            },
            767 : {
                items: 3,
            }
        }
    });
}

window.stopPartCarousel = function() {
    var owl = $('.partnears__carousel');
    owl.trigger('destroy.owl.carousel');
    owl.addClass('disabledCar');
}



//News carousell
window.newsCarousell = function() {
    var owl = $(".news__carousel").owlCarousel({
        smartSpeed: 1000,
        mouseDrag: true,
        autoplay: false,
        center: false,
        addClassActive : true,
        loop: true,
        dots: false,
        nav: true,
        dotsEach: 1,
        touchDrag: true,
        dotsSpeed: 1000,
        navSpeed: 1000,
        stagePadding: 0,
        margin: 30,
        responsive : {
            0 : {
                items: 1,
            },
            760 : {
                items: 2,
            },
            960 : {
                items: 3,
            },
            1290 : {
                items: 4,
            }
        }
    });
}


window.openSelect = function($this) {
    var
        thisList = $this.next('.selector__list');

    $this.closest('.label').siblings().find('.selector__list').slideUp(300);
    $this.closest('.label').siblings().find('.selector__ctr').removeClass('opened');
    $this.closest('.label').siblings().find('.selector').removeClass('openedSel');
    thisList.slideToggle(300);


    if($this.hasClass('opened')) {
        $this.removeClass('opened');
        setTimeout(function () {
            $this.closest('.selector').removeClass('openedSel');
        },300);
    } else {
        $this.addClass('opened');
        $this.closest('.selector').addClass('openedSel');
    }

    $(document).click(function(){
        $('.selector__list').slideUp(300);
        $('.selector__ctr').removeClass('opened');
    });

    $('.selector').click(function(e){
        e.stopPropagation();
    });

    $('.error').removeClass('error');
}


window.changeSel = function($this) {
    var
        thisText = $this.next('span').text(),
        thisList = $this.closest('.selector__list'),
        thisCont = $this.closest('.selector'),
        thisCtrl = thisCont.find('.selector__ctr');

    setTimeout(function () {
        $this.closest('.label').find('.selector').removeClass('openedSel');
    },300)

    thisCtrl.text(thisText);
    thisList.slideUp(300);
    thisCtrl.removeClass('opened');
}


window.changeCity = function($this) {
    var
        thisText = $this.text(),
        thisCont = $this.closest('.select__city'),
        thisCtrl = thisCont.find('input');

    thisCtrl.val(thisText);
}


window.openPopup = function($this) {
    event.preventDefault();

    var
        href = $this.attr('href'),
        popup = $(href),
        sctollTop = $(window).scrollTop();

    console.log(sctollTop);

    popup.fadeIn(400);
    $('body').addClass('overHide');
}

window.openPopupByData = function($this) {
    event.preventDefault();

    var
        href = $this.attr('data-href'),
        popup = $(href);

    popup.fadeIn(400);
    $('body').addClass('overHide');
}

window.closePopup = function() {
    event.preventDefault();

    $('.popup').fadeOut(400);
    $('body').removeClass('overHide');
}

window.closePopupHelp = function($this) {
    event.preventDefault();

    var
        id = $this.data().id,
        cont = $('#'+id).find('.help__bottom'),
        btn = $('#'+id).find('.help__buttonb a');

    $('.popup').fadeOut(400);
    $('body').removeClass('overHide');

    setTimeout(function () {
        cont.removeClass('showedContacrs');
        btn.show();
    },500);
}


window.openFilter = function($this) {
    event.preventDefault();

    var
        thisList = $this.next('.selector__list');

    $this.closest('li').siblings().find('.selector__list').slideUp(300);
    $this.closest('li').siblings().find('.filter__ctrl').removeClass('opened');
    $this.toggleClass('opened');
    thisList.slideToggle(300);

    $(document).click(function(){
        $('.selector__list').slideUp(300);
        $('.filter__ctrl').removeClass('opened');
    });

    $('.filterItemSelect').click(function(e){
        e.stopPropagation();
    });
}


window.changeFilter = function($this) {
    var
        thisList = $this.closest('.selector__list'),
        thisCont = $this.closest('.filter__ctrlcont'),
        thisCtrl = thisCont.find('.filter__ctrl');

    var
        arr = '';

    thisList.find('input:checked').each(function (i) {
       var
           inp = $(this),
           chT = inp.next('span').text();

       if(i === 0) {
           arr += chT;
       } else {
           arr += ', &nbsp;'+chT;
       }
    });

    if(thisList.find('input:checked').length > 0) {
        thisCtrl.html(arr);
    } else {
        var
            pl = thisCtrl.data().st;

        thisCtrl.html(pl);
    }
}


window.openVideo = function($this) {
    event.preventDefault();
    var
        thisVideo = $this.data().video;

    $('.videoPopEmbed').html('<iframe src="https://www.youtube.com/embed/'+thisVideo+'" frameborder="0"></iframe>');

    setTimeout(function () {
        $('#videoPopup').fadeIn(400);
        $('body').addClass('overHide');
    },300);
};

window.videoClose = function() {
    event.preventDefault();

    $('#videoPopup').fadeOut(400);
    $('body').removeClass('overHide');

    setTimeout(function () {
        $('.videoPopEmbed').empty();
    },300);
};

window.openFilterBtn = function($this) {
    event.preventDefault();

    if(!$this.hasClass('opened')) {
        setTimeout(function () {
            $this.addClass('opened');
            $('.filter__list').slideDown(300);
        },50);
    } else {
        setTimeout(function () {
            $this.removeClass('opened');
            $('.filter__list').slideUp(300);
        },50);
    }
}




////////Animations
window.animation = function() {
        var
        wHeight = $(window).height();

    $('.fadeAnim').each(function () {
        var
            $this = $(this),
            thisTop = $this.offset().top;

        if (thisTop < wHeight) {
            $this.addClass('fadeDone');

            setTimeout(function () {
                $this.removeClass('fadeAnim');
            }, 1000);
        }
    });

    $(window).scroll(function () {
        var
            wScroll = $(this).scrollTop();

        $('.fadeAnim').each(function () {
            var
                wBorder = wScroll + wHeight - 50,
                $this = $(this),
                thisTop = $this.offset().top;

            if (thisTop < wBorder) {
                $this.addClass('fadeDone');

                setTimeout(function () {
                    $this.removeClass('fadeAnim ');
                }, 1000);
            }
        });

    });
}


////////

window.changeSum = function() {
    $('#donate__sum').val('');
}

window.changeSumInput = function() {
    $('.donate__input:checked').prop('checked', false);
}

window.changeTab = function($this) {
    event.preventDefault();

    var
        href = $this.attr('href'),
        tab = $(href);

    $this.addClass('active');
    $this.closest('li').siblings().find('a').removeClass('active');

    tab.addClass('active');
    tab.siblings().removeClass('active');

    if($this.hasClass('donatCtrl')) {
        var
            img =   $this.data().img;

        $('#'+img).addClass('active');
        $('#'+img).siblings().removeClass('active');
    }
}

window.copyText = function($this) {
    event.preventDefault();

    var
        copyText = $this.prev('.crpt__copytext'),
        help = $this.find('.copyed');

    navigator.clipboard.writeText(copyText.text());
    help.fadeIn(300);

    setTimeout(function () {
        help.fadeOut(300);
    }, 1000);
}



//////
window.openContact = function($this) {
    event.preventDefault();

    var
        contacts = $this.closest('.help__buttonb').prev('.help__bottom');

    contacts.addClass('showedContacrs');
    $this.hide();

}



////////
window.stepMove = function($this) {
    event.preventDefault();

    var
        href = $this.attr('href'),
        thisStep = $(href),
        stepText = thisStep.data().text,
        stepUrl = $('.help__url[href="'+href+'"]');

    thisStep.addClass('active');
    stepUrl.addClass('active');
    thisStep.siblings().removeClass('active');
    stepUrl.closest('li').siblings().find('a').removeClass('active');

    stepUrl.closest('li').nextAll().find('a').removeClass('done');

    progresBar(href);
    progresText(stepText);
}


window.moveStepNav = function($this) {
    event.preventDefault();

    var
        href = $this.attr('href'),
        thisStep = $(href),
        stepText = thisStep.data().text;

    if($this.hasClass('done') && !$this.hasClass('unclicked')) {
        thisStep.addClass('active');
        $this.addClass('active');
        thisStep.siblings().removeClass('active');
        $this.closest('li').siblings().find('a').removeClass('active');

        $this.closest('li').nextAll().find('a').removeClass('done');

        progresBar(href);
        progresText(stepText);
    }
}


window.progresBar = function(href) {

    var
        num = href.substr(-1),
        parcent = 100 / 4 * num;

    if(num === 5) {
        $('.help__progres').hide();
    }

    $('.progRtap').text(num);
    $('.progress__line').css({'width' : parcent+'%'});

}

window.progresText = function(stepText) {

    $('.help__text').text(stepText);

}


window.nextStep = function($this) {
    event.preventDefault();
    var
        href = $this.attr('href'),
        thisStep = $(href),
        stepText = thisStep.data().text,
        stepUrl = $('.help__url[href="'+href+'"]');

    var errorsCount = 0;

    if (errorsCount === 0) {
        if(!thisStep.hasClass('lastStep')) {
            thisStep.addClass('active');
            stepUrl.addClass('active');
            thisStep.siblings().removeClass('active');
            stepUrl.closest('li').siblings().find('a').removeClass('active');

            stepUrl.closest('li').prev().find('a').addClass('done');

            progresBar(href);
            progresText(stepText);
        } else {
            $this.next('button').click();
        }
    }

    $('input, textarea').focusin(function () {
        $('.error').removeClass('error');
    });

    $('input').change(function () {
        $('.error').removeClass('error');
    });
};


window.isValidEmailAddress = function(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
};


window.closePopupReq = function($this) {
    event.preventDefault();

    var
        id = $this.data().id,
        popup = $('#'+id),
        firstText = popup.find('.firstStep').data().text;

    $('.popup').fadeOut(400);
    $('body').removeClass('overHide');

    if(popup.find('.lastStep').hasClass('active')) {
        setTimeout(function () {
            popup.find('.help__url').removeClass('done');
            popup.find('.help__url').removeClass('unclicked');
            popup.find('.help__url').removeClass('active');
            popup.find('.help__navitem:first-child .help__url').addClass('active');
            popup.find('.help__progres').show();
            popup.find('.help__step').removeClass('active');
            popup.find('.firstStep').addClass('active');
            popup.find('.help__text').text(firstText);
        }, 500);
    }
}




//////////////////
window.submitForm = function($this) {
    event.preventDefault();

    var
        thisForm = $this.closest('form'),
        thisRequired = thisForm.find('input[required], textarea[required], .selector__list');

    var errorsCount = 0;

    thisRequired.each(function () {
        var
            $this = $(this),
            inputVal = $this.val(),
            reqType = $this.attr('type');

        if (reqType === 'tel') {
            if (inputVal.length !== 17) {
                errorsCount += 1;

                $this.closest('.field__label').addClass('error');
            }
        }
        if ($this.hasClass('emailReq')) {
            if (!isValidEmailAddress(inputVal)) {
                errorsCount += 1;

                $this.closest('.field__label').addClass('error');
            }
        }
        if (reqType === 'text' || $this.hasClass('field__textarea')) {
            if (inputVal.length < 1) {
                errorsCount += 1;

                $this.closest('.field__label').addClass('error');
            }
        }
        if ($this.hasClass('required')) {
            var checked = $this.find('input:checked');
            if (checked.length < 1) {
                errorsCount += 1;

                $this.closest('.field__label').addClass('error');
            }
        }
        if (reqType === 'checkbox') {
            if ($this.is(":not(:checked)")) {
                errorsCount += 1;

                $this.closest('.field__label').addClass('error');
            }
        }

    });

    if($this.hasClass('donate__button')) {
        var
            radio = $('input[name="donate__sumchange"]:checked').length,
            sum = $('input[name="sum"]').val().length,
            href = $this.attr('href'),
            inp = $(href);

        inp.prop('checked', true);

        if(radio === 0 && sum === 0) {
            errorsCount += 1;
            $('.danate__sumcont').addClass('error');
        }
    }

    if (errorsCount === 0) {
        $this.next('button').click();
    }

    $('input, textarea').focusin(function () {
        $('.error').removeClass('error');
    });

    $('input').change(function () {
        $('.error').removeClass('error');
    });
};



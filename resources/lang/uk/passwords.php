<?php

/*
|--------------------------------------------------------------------------
| Password Reset Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are the default lines which match reasons
| that are given by the password broker for a password update attempt
| has failed, such as for an invalid token or invalid new password.
|
*/

return [
    'reset'     => 'Ваш пароль змінено!',
    'sent'      => 'Ми надіслали на Вашу електронну адресу посилання для зміни пароля!',
    'throttled' => 'Зачекайте, перш ніж повторити спробу.',
    'token'     => 'Нам дуже прикро, але термін дії посилання для відновлення пароля вже закінчився. Спробуй знову відправити посилання, але в цей раз будь прудкіше.',
    'user'      => 'Користувача із вказаною електронною адресою не знайдено.',
    'Whoops! Something went wrong.'      => 'Ой! Щось пішло не так',
];

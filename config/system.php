<?php

return [
    'count_items_in_page' => 30,
    'timezone' => 3,
    'date_format' => "Y-m-d",
    'date_time_format' => "Y-m-d H:i",
    'time_format' => "H:i:s",
];

<?php

use App\Http\Controllers\Admin\AdminLoginController;
use App\Http\Controllers\Admin\ComplaintController;
use App\Http\Controllers\Admin\IndexController;
use App\Http\Controllers\Admin\SupportController;
use App\Http\Controllers\Admin\TestController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function (){
    Route::get('login', [AdminLoginController::class, 'showLoginForm'])->name('login-form');
    Route::post('login', [AdminLoginController::class, 'login'])->name('login-handler');
    Route::post('logout', [AdminLoginController::class, 'logout'])->name('logout');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth:admin']], function (){
    Route::get('/test', [TestController::class, 'index']);
    Route::get('/', [IndexController::class, 'index'])->name('index');
    Route::group(['prefix' => 'supports', 'as' => 'supports.'], function (){
        Route::get('/', [SupportController::class, 'index'])->name('index');
        Route::get('/{support}', [SupportController::class, 'show'])->name('show');
        Route::get('/change-status/{support}', [SupportController::class, 'changeStatus'])->name('change-status');
        Route::delete('/{support}', [SupportController::class, 'delete'])->name('delete');
    });
    Route::group(['prefix' => 'users', 'as' => 'users.'], function (){
        Route::get('/', [UserController::class, 'index'])->name('index');
        Route::get('/{user}', [UserController::class, 'edit'])->name('edit');
        Route::put('/{user}', [UserController::class, 'update'])->name('update');
        Route::delete('/{user}', [UserController::class, 'delete'])->name('delete');
    });
    Route::group(['prefix' => 'complaints', 'as' => 'complaints.'], function (){
        Route::get('/', [ComplaintController::class, 'index'])->name('index');
        Route::get('/change-status/{complaint}', [ComplaintController::class, 'changeStatus'])->name('change-status');
        Route::delete('/{complaint}', [ComplaintController::class, 'delete'])->name('delete');
    });
});

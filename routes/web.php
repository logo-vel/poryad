<?php

use App\Http\Controllers\Admin\AdminLoginController;
use App\Http\Controllers\Admin\IndexController;
use App\Http\Controllers\Admin\SupportController;
use App\Http\Controllers\Admin\TestController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\FrontPageController::class, 'index'])->name('home');
Route::get('/', [\App\Http\Controllers\FrontPageController::class, 'index'])->name('login');
Route::inertia('/about', 'About');
Route::inertia('/polityka_konfidentsiynosti', 'TermsOfService');

Route::post('/sendCode', [\App\Http\Controllers\ClientController::class, 'sendCode']);
Route::post('/checkCode', [\App\Http\Controllers\ClientController::class, 'checkCode'])->name('check-code');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    // Route::get('/dashboard', [\App\Http\Controllers\ClientController::class, 'index'])->name('dashboard');
//    Route::post('/makeRequest', [\App\Http\Controllers\ClientController::class, 'makeRequest']);
    Route::get('/profile', [\App\Http\Controllers\ClientController::class, 'index'])->name('dashboard');
    Route::get('/logout', [\App\Http\Controllers\ClientController::class, 'destroy']);
    Route::get('/delete', [\App\Http\Controllers\Api\SupportController::class, 'destroy']);
    Route::post('/edit-user', [\App\Http\Controllers\ClientController::class, 'update']);
    Route::post('/deleteRequest', [\App\Http\Controllers\ClientController::class, 'deleteRequest']);
    Route::post('/offRequest', [\App\Http\Controllers\ClientController::class, 'offRequest']);
    Route::post('/editRequest', [\App\Http\Controllers\ClientController::class, 'editRequest']);
});

Route::post('/step-first', [\App\Http\Controllers\ClientController::class, 'stepFirst']);
Route::post('/step-second', [\App\Http\Controllers\ClientController::class, 'stepSecond']);
Route::post('/step-third', [\App\Http\Controllers\ClientController::class, 'stepThird']);
Route::post('/step-fourth', [\App\Http\Controllers\ClientController::class, 'stepFourth']);
Route::post('/makeRequest', [\App\Http\Controllers\ClientController::class, 'makeRequest']);
Route::post('/makeDonate', [\App\Http\Controllers\ClientController::class, 'makeDonate']);
Route::get('/requests', [\App\Http\Controllers\CatalogController::class, 'byAll'])->name('requests');
Route::get('/{city:slug}', [\App\Http\Controllers\CatalogController::class, 'byCity'])->name('city');
Route::get('/d/{category:slug}', [\App\Http\Controllers\CatalogController::class, 'byCategory'])->name('category');
Route::get('/{city:slug}/{category_slug}', [\App\Http\Controllers\CatalogController::class, 'byCityCategory'])->name('category-with-city');

Route::get('/feedback', [\App\Http\Controllers\FeedbackController::class, 'index']);
Route::post('/feedback', [\App\Http\Controllers\FeedbackController::class, 'store']);

Route::post('/complaint', [\App\Http\Controllers\ComplaintController::class, 'store']);

Route::post('/password/update/custom', [\App\Http\Controllers\Auth\NewPasswordController::class, 'store']);
Route::post('forgot-password', [PasswordResetLinkController::class, 'store'])->name('password.email');

Route::get('/register', function(){
    return Redirect::to('/', 301);
});
Route::get('/login', function(){
    return Redirect::to('/', 301);
});



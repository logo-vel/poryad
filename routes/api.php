<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function (){

    Route::apiResources([
        'community' => \App\Http\Controllers\Api\CommunityController::class,
        'region' => \App\Http\Controllers\Api\RegionController::class,
        'city' => \App\Http\Controllers\Api\CityController::class,
        'support_status' => \App\Http\Controllers\Api\SupportStatusController::class,
        'support_category' => \App\Http\Controllers\Api\SupportCategoryController::class,
        'support' => \App\Http\Controllers\Api\SupportController::class,
    ]);
});


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

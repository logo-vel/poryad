<?php

namespace App\Interfaces;

interface SupportRepositoryInterface
{
    public function getAll();
    public function getById(int $id);
    public function getByLimit(int $limit);
    public function getByCity(int $id);
    public function getPaginateByCity(int $id, int $per_page);
    public function getByCategory(int $id);
    public function getPaginateByCategory(int $id, int $per_page);
    public function getByCityAndCategory(int $city_id, int $category_id);
    public function getPaginateByCityAndCategory(int $city_id, int $category_id, int $per_page);
    public function getPaginateByUser(int $id, int $per_page);
}

<?php

namespace App\Providers;

use App\Http\ViewComposers\CommonComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        View::composer('*', CommonComposer::class);
    }
}

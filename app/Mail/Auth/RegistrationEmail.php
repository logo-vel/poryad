<?php

namespace App\Mail\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;

class RegistrationEmail extends Mailable
{
    use Queueable, SerializesModels;


    public $user;

    public $password;

    public $phone;
    /**
     * Create a new message instance.
     *
     * @return RegistrationEmail
     */
    public function __construct(User $user, $password, $phone)
    {
        $this->user = $user;
        $this->password = $password;
        $this->phone = $phone;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.auth.registration')
        ->subject("Доступи до особистого кабінету")
        ->with(['phone' => $this->phone]);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Admin\SupportAPHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ChangeStatusSupportRequest;
use App\Models\Support;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    public $helper;

    public function __construct(SupportAPHelper $helper)
    {
        $this->helper = $helper;
    }

    public function index()
    {
        try {
            $vars = $this->helper->getIndexVars();
        } catch (\DomainException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return view('admin.supports.index', $vars);
    }

    public function show(Support $support)
    {
        try {
            $vars = $this->helper->getShowVars($support);
        } catch (\DomainException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return view('admin.supports.show', $vars);
    }

    public function delete(Support $support)
    {
        try {
            $support->deleteObj();
        } catch (\DomainException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Success deleted!');
    }

    public function changeStatus(Support $support, ChangeStatusSupportRequest $request)
    {
        try {
            $support->changeStatus($request->status);
        } catch (\DomainException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Success updated!');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Admin\ComplaintAPHelper;
use App\Helpers\Admin\UserAPHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ChangeStatusComplaintRequest;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\Models\Complaint;
use App\Models\User;

class ComplaintController extends Controller
{
    public $helper;

    public function __construct(ComplaintAPHelper $helper)
    {
        $this->helper = $helper;
    }

    public function index()
    {
        try {
            $vars = $this->helper->getIndexVars();
        } catch (\DomainException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return view('admin.complaints.index', $vars);
    }

    public function changeStatus(Complaint $complaint, ChangeStatusComplaintRequest $request)
    {
        try {
            $complaint->changeStatus($request->status);
        } catch (\DomainException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Success updated!');
    }

    public function delete(Complaint $complaint)
    {
        try {
            $complaint->deleteObj();
        } catch (\DomainException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Success deleted!');
    }
}

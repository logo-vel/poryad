<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Admin\UserAPHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\Models\User;

class UserController extends Controller
{
    public $helper;

    public function __construct(UserAPHelper $helper)
    {
        $this->helper = $helper;
    }

    public function index()
    {
        try {
            $vars = $this->helper->getIndexVars();
        } catch (\DomainException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return view('admin.users.index', $vars);
    }

    public function edit(User $user)
    {
        try {
            $vars = $this->helper->getEditVars($user);
        } catch (\DomainException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return view('admin.users.edit', $vars);
    }

    public function update(User $user, UpdateUserRequest $request)
    {
        try {
            $user->updateObj($request);
        } catch (\DomainException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Success updated!');
    }

    public function delete(User $user)
    {
        try {
            $user->deleteObj();
        } catch (\DomainException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }

        return redirect()->back()->with('success', 'Success deleted!');
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\HtmlHelpers\ViewAdminHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\AdminLoginRequest;
use App\UseCases\Auth\AuthAPService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    protected $redirectTo = '/admin';

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }

    public function login(Request $request)
    {
        try {
            if (!Auth::guard('admin')->attempt([
                'email' => $request->get('email'),
                'password' => $request->get('password')
            ], $request->filled('remember'))) {
                throw new \DomainException('Bad login');
            }
        } catch (\DomainException $e) {
            return back()->withInput($request->only('email', 'remember'));
        }

        return redirect()->intended(route('admin.index'));
    }

    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect()->route('admin.index');
    }
}

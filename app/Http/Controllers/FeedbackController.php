<?php

namespace App\Http\Controllers;

use App\Http\Requests\FeedbackRequest;
use stdClass;
use App\Mail\FeedbackMailer;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    public function index()
    {

    }

    public function store(FeedbackRequest $request)
    {
        $data = new stdClass();
        $data->first_name = $request->first_name;
        $data->email = $request->email;
        $data->message = $request->message;
        Mail::to('info@poryad.in.ua')->send(new FeedbackMailer($data));

        return back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\ComplaintRequest;
use App\Models\Complaint;

class ComplaintController extends Controller
{
    public function store(ComplaintRequest $request)
    {
        try {
            Complaint::createObj($request->support_id, $request->text);
        } catch (\DomainException $e) {
            return back();
        }

        return back();
    }
}

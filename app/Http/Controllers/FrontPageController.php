<?php

namespace App\Http\Controllers;

use App\Http\Resources\Api\SupportCategoryResource;
use App\Http\Resources\Api\SupportResource;
use App\Models\Support;
use App\Models\SupportCategory;
use App\Models\User;
use App\Repositories\SupportRepository;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class FrontPageController extends Controller
{
    private SupportRepository $repository;

    public function __construct(SupportRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return Inertia::render('Welcome', [
            'open_popup' => session('open_popup'),
            'meta' => [
                'title' => '',
                'description' => '',
            ],
            'supports' => SupportResource::collection($this->repository->getByLimit(12))
        ]);
    }
}

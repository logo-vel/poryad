<?php

namespace App\Http\Controllers;

use App\Http\Resources\Api\SupportResource;
use App\Models\City;
use App\Models\SupportCategory;
use App\Repositories\SupportRepository;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CatalogController extends Controller
{
    private SupportRepository $repository;

    public function __construct(SupportRepository $repository)
    {
        $this->repository = $repository;
    }

    public function byAll(Request $request)
    {
        $reset_url = route('requests');
        $degrees = $this->repository->getDegreeByAllFilter($request);
        $supports = $this->repository->getByAllFilter($request, (int) $request->get('per_page', 10));
        return Inertia::render('Requests', compact('supports', 'degrees', 'reset_url'));
    }

    public function byCity(Request $request, City $city)
    {
        $reset_url = route('city', ['city' => $city]);
        $degrees = $this->repository->getDegreeByCityFilter($request, $city->id);
        $supports = $this->repository->getPaginateByCityFilter($request, $city->id, (int) $request->get('per_page', 10));
        return Inertia::render('City', compact('city', 'supports', 'degrees', 'reset_url'));
    }

    public function byCategory(Request $request, SupportCategory $category)
    {
        $reset_url = route('category', ['category' => $category]);
        $degrees = $this->repository->getDegreeByCategoryFilter($request, $category->id);
        $supports = $this->repository->getPaginateByCategoryFilter($request, $category->id, (int) $request->get('per_page', 10));
        return Inertia::render('Category', compact('category', 'supports', 'degrees', 'reset_url'));
    }

    public function byCityCategory(Request $request, City $city, $category_slug)
    {
        $reset_url = route('category-with-city', ['city' => $city, 'category_slug' => $category_slug]);
        $category = SupportCategory::whereSlug($category_slug)->first();
        $supports = $this->repository->getPaginateByCityAndCategoryFilter($request, $city->id, $category->id, (int) $request->get('per_page', 10));
        return Inertia::render('Category', compact('city', 'category', 'supports', 'reset_url'));
    }
}

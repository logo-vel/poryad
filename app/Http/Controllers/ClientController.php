<?php

namespace App\Http\Controllers;

use App\Events\Auth\UserRegistered;
use App\Http\Requests\MakeRequest;
use App\Http\Requests\UpdateSupportRequest;
use App\Models\Support;
use App\Models\SupportStatus;
use App\Models\User;
use App\Models\UserCode;
use App\Repositories\SupportRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Illuminate\Validation\Rules;


class ClientController extends Controller
{
    private SupportRepository $repository;

    public function __construct(SupportRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $supports = $this->repository->getPaginateByUser(Auth::id());
        return Inertia::render('Profile', compact('supports'));
    }

    public function sendCode(Request $request)
    {
        $data = $request->validate([
            'phone' => 'required|min:17|max:17' // 380950464375
        ]);
        $code = rand(100000, 999999);
        $symbols = array('+', '-', '(', ')');
        $userCode = UserCode::create([
            'phone' => str_replace($symbols, '', $data['phone']),
            'code' => $code,
        ]);
//        $res = Http::withToken('32fc49f7c6f74ac8497d656b1eb59b03c051e405')->get('https://api.turbosms.ua/message/send.json', [
//            'sms' => [
//                'sender' => 'TAXI',
//                'text' => "Login code: $code",
//            ],
//            'recipients' => [
//                $data['phone']
//            ]
//        ]);
        return redirect()->back();
    }

    public function checkCode(Request $request)
    {
//        dd($request);

        $data = $request->validate([
            'phone' => 'required|min:17|max:17',
            'code' => 'required|min:8'
        ]);

//        dd($data);
        $symbols = array('+', '-', '(', ')');
        $phone = str_replace($symbols, '', $data['phone']);

        $user = User::where('phone', $phone)->first();

        $id = (int) $user->id;

        if($user && Hash::check($request->get('code'), $user->password)){

            Auth::loginUsingId($id);
            return redirect('/profile');
        } else {
            return back()->withErrors(['code'=>__('auth.failed')]);
        }
    }

    public function stepFirst(Request $request)
    {
        $symbols = array('+', '-', '(', ')', '@');
        $phone = str_replace($symbols, '', request('phone'));

        $request->validate([
            'name' => 'required',
            'phone' => 'required|min:17|max:17',
            'email' => 'required|email',
        ]);

        $user = User::where('phone', $phone)->first();

        if ($user === null) {
            $request->validate([
            'email' => 'unique:users'
            ]);
        }

        return back();
    }

    public function stepSecond(Request $request)
    {
//        dd($request);
        if(request('viber') != null) {
            $request->validate([
                'viber' => 'required|min:17|max:17'
            ]);
        }

        return back();
    }

    public function stepThird(Request $request)
    {

        $request->validate([
            'city_id' => 'required',
            'category_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'degree' => 'required',
        ]);

        return back();
    }

    public function stepFourth(Request $request)
    {

//        dd($request);
        if(request('delivery') === true) {
            $request->validate([
                'delivery_type' => 'required',
                'delivery_address' => 'required',
            ]);
        }

            $request->validate([
                'terms' => ['boolean', 'required', Rule::in([true])],
            ]);

        return back();
    }

    public function makeDonate(Request $request)
    {

        $data = $request->validate([
            'amount' => 'required',
            'description' => 'required',
            'email' => 'required',
//            'action' => 'required',
            'sender_first_name' => 'required',
        ]);

//        dd($data['action']);

//        $data['public_key'] = 'i54998125706';
//        $data['private_key'] = 'CKU1BMExCirXtxVjwfYRKsGurhd0Uh9q9sx9m0mE';
//        $data['version'] = '3';
//        $data['action'] = 'paydonate';
          $account  = 'poryad_in_ua';
          $domain_name = 'poryad.in.ua';
          $currency = 'UAH';
          $order_id = 'i54998125706' . $data['amount'];
          $time = date("Ymd");
          $product = 'Благодійний внесок';
          $quantity = '1';
          $amount = $data['amount'];
          $name = $data['sender_first_name'];
          $email = $data['email'];
          $phone = $data['description'];

//        $value = array(
//             'public_key' => $data['public_key'],
//             'version'  => $data['version'],
//             'action' => $data['action'],
//             'amount' => $data['amount'],
//             'currency' => $data['currency'],
//             'sender_first_name' =>  $data['sender_first_name'],
//             'description' => 'ПІБ: ' . $data['sender_first_name'] . ' Тел.: ' . $data['description'] . ' Email: ' . $data['email'],
//             'order_id' =>  'i54998125706' . $data['amount']
//        );
//
//        $value = json_encode($value);
//
//        $data_string = base64_encode($value);
//
//        $sign =  $data['private_key'] . $data_string . $data['private_key'];

        $string = $account . ";" . $domain_name . ";" . $order_id . ";" . $time . ";" . $amount . ";" . $currency . ";" . $product . ";" . $quantity . ";"  . $amount;
        $key = "c98801d3b0c7e2306a2a556b8edb54bbfce33036";
        $hash = hash_hmac("md5",$string,$key);

//        $signature = base64_encode(sha1($sign, true));

//       dd($string);

        return response()->json([
//            'data' => $data_string,
//            'signature' => $signature,
            'reference' => $order_id,
            'signature' => $hash,
            'time' => $time,
            'amount' => $amount,
            'name' => $name,
            'phone' => $phone,
            'email' => $email,
        ]);
    }

    public function makeRequest(Request $request)
    {

        $symbols = array('+', '-', '(', ')', '@');
        $phone = str_replace($symbols, '', request('phone'));

        if(request('is_phone_viber') === true) {
            $viber = str_replace($symbols, '', request('phone'));
        } elseif(request('viber') != null) {
            $viber = str_replace($symbols, '', request('viber'));
        } else {
            $viber = null;
        }

        if(request('is_phone_tg') === true) {
            $telegram = '+' . str_replace($symbols, '', request('phone'));
        } elseif(request('telegram') != null) {
            $telegram = str_replace($symbols, '', request('telegram'));
        } else {
            $telegram = null;
        }

        $password = bin2hex(openssl_random_pseudo_bytes(4));
        $email = request('email');

        $dataUser = [
            'name' => request('name'),
            'phone' => $phone,
            'email' => strtolower($email),
            'telegram' => $telegram,
            'viber' => $viber,
            'password' => Hash::make($password),
            'city_id' => request('city_id')
        ];

//        $user = User::where('phone', $phone)->orWhere('email', $dataUser['email'])->first();

        $user = User::where('phone', $phone)->first();

        if ($user === null) {
            $user = User::create($dataUser);
            event(new UserRegistered($user, $password, $phone));
        }

        $dataAdd = [
            'status_id' => SupportStatus::whereIsDefault(true)->first()->id,
            'city_id' => request('city_id'),
            'category_id' => request('category_id'),
            'needy_user_id' => $user->id,
            'status' => true,  // тут надо поменять на false перед запуском
            'title' => request('title'),
            'description' => request('description'),
            'degree' => request('degree'),
            'delivery' => request('delivery'),
            'delivery_type' => request('delivery_type'),
            'delivery_address' => request('delivery_address'),
            'is_other_needy' => request('is_other_needy'),
            'name' => request('name'),
            'phone' => $phone,
            'telegram' => $telegram,
            'viber' => $viber,
            'email' => strtolower($email),
        ];

        Support::create(array_merge($dataAdd));

        return back();
    }


    public function deleteRequest(Request $request)
    {
        $data = $request->validate([
            'support_id' => 'required',
        ]);
        Support::where('id', $data['support_id'])->delete();

        return back();
    }

    public function offRequest(Request $request)
    {
        $data = $request->validate([
            'support_id' => 'required',
        ]);
        Support::where('id', $data['support_id'])->update(['status' => false]);

        return back();
    }

    public function editRequest(UpdateSupportRequest $request)
    {
        $data = $request->except('is_phone_viber', 'terms', 'needy_user_id');

        $symbols = array('+', '-', '(', ')');
        $data['phone'] = str_replace($symbols, '', request('phone'));
        if(request('is_phone_viber') === true) {
            $data['viber'] = str_replace($symbols, '', request('phone'));
        } else {
            $data['viber'] = str_replace($symbols, '', request('viber'));
        }

        Support::where('id', $request->id)->update($data);

        return back();
    }

    public function update(Request $request): \Illuminate\Http\RedirectResponse
    {
        $user = Auth::user();

        $request->validate([
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'viber' => 'nullable',
            'telegram' => 'nullable',
            'password' => ['nullable', 'confirmed', Rules\Password::defaults()],
            'password_confirmation' => 'nullable',
        ]);

        $symbols = array('+', '-', '(', ')');
        $phone = str_replace($symbols, '', request('phone'));
        $viber = str_replace($symbols, '', request('viber'));

        $user->name = request('name');

        if($phone != $user['phone']) {
            $request->validate(['phone'=>'unique:users']);
            $user->phone = $phone;
        }

        if(request('email') != $user['email']) {
            $request->validate(['email'=>'unique:users']);
            $user->email = request('email');
        }

        $user->viber = $viber;


        $user->telegram = request('telegram');


        if(request('password') && request('password_confirmation')) {
            $request->validate([
                'password'         => 'required|min:8',
                'password_confirmation'  => 'required|same:password'
            ]);

            $user->password = Hash::make(request('password'));
        }


        $user->save();

        return back();
    }

    public function destroy(Request $request): \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    {


        Auth::guard('web')->logout();



        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}

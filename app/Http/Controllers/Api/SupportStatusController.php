<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreSupportStatusRequest;
use App\Http\Requests\Api\UpdateSupportStatusRequest;
use App\Http\Resources\Api\SupportStatusResource;
use App\Models\SupportStatus;
use function response;

class SupportStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return SupportStatusResource::collection(SupportStatus::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Api\StoreSupportStatusRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreSupportStatusRequest $request)
    {
        return response()->json(SupportStatus::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SupportStatus  $supportStatus
     * @return SupportStatusResource
     */
    public function show(SupportStatus $supportStatus)
    {
        return new SupportStatusResource($supportStatus);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Api\UpdateSupportStatusRequest  $request
     * @param  \App\Models\SupportStatus  $supportStatus
     * @return \App\Http\Resources\Api\SupportStatusResource
     */
    public function update(UpdateSupportStatusRequest $request, SupportStatus $supportStatus)
    {
        return new SupportStatusResource($supportStatus->update($request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SupportStatus  $supportStatus
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(SupportStatus $supportStatus)
    {
        return response()->json($supportStatus->delete());
    }
}

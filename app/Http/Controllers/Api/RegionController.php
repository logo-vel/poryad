<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreRegionRequest;
use App\Http\Requests\Api\UpdateRegionRequest;
use App\Http\Resources\Api\RegionCollection;
use App\Http\Resources\Api\RegionResource;
use App\Models\Region;
use function response;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return RegionCollection
     */
    public function index()
    {
        return new RegionCollection(Region::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Api\StoreRegionRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRegionRequest $request)
    {
        return response()->json(Region::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Region  $region
     * @return RegionResource
     */
    public function show(Region $region)
    {
        return new RegionResource($region);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Api\UpdateRegionRequest  $request
     * @param  \App\Models\Region  $region
     * @return \App\Http\Resources\Api\RegionResource
     */
    public function update(UpdateRegionRequest $request, Region $region)
    {
        return new RegionResource($region->update($request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Region  $region
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Region $region)
    {
        return response()->json($region->delete());
    }
}

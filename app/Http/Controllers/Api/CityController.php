<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreCityRequest;
use App\Http\Requests\Api\UpdateCityRequest;
use App\Http\Resources\Api\CityCollection;
use App\Http\Resources\Api\CityResource;
use App\Models\City;
use function response;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \App\Http\Resources\Api\CityCollection
     */
    public function index()
    {
        return new CityCollection(City::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Api\StoreCityRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCityRequest $request)
    {
        return response()->json(City::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\City  $city
     * @return CityResource
     */
    public function show(City $city)
    {
        return new CityResource($city);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Api\UpdateCityRequest  $request
     * @param  \App\Models\City  $city
     * @return CityResource
     */
    public function update(UpdateCityRequest $request, City $city)
    {
        return new CityResource($city->update($request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\City  $city
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(City $city)
    {
        return response()->json($city->delete());
    }
}

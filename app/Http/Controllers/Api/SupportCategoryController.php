<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreSupportCategoryRequest;
use App\Http\Requests\Api\UpdateSupportCategoryRequest;
use App\Http\Resources\Api\SupportCategoryResource;
use App\Models\SupportCategory;
use function response;

class SupportCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return SupportCategoryResource::collection(SupportCategory::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Api\StoreSupportCategoryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreSupportCategoryRequest $request)
    {
        return response()->json(SupportCategory::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SupportCategory  $supportCategory
     * @return \App\Http\Resources\Api\SupportCategoryResource
     */
    public function show(SupportCategory $supportCategory)
    {
        return new SupportCategoryResource($supportCategory);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Api\UpdateSupportCategoryRequest  $request
     * @param  \App\Models\SupportCategory  $supportCategory
     * @return \App\Http\Resources\Api\SupportCategoryResource
     */
    public function update(UpdateSupportCategoryRequest $request, SupportCategory $supportCategory)
    {
        return new SupportCategoryResource($supportCategory->update($request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SupportCategory  $supportCategory
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(SupportCategory $supportCategory)
    {
        return response()->json($supportCategory->delete());
    }
}

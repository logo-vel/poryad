<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreCommunityRequest;
use App\Http\Requests\Api\UpdateCommunityRequest;
use App\Http\Resources\Api\CommunityCollection;
use App\Http\Resources\Api\CommunityResource;
use App\Models\Community;
use function response;

class CommunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \App\Http\Resources\Api\CommunityCollection
     */
    public function index()
    {
        return new CommunityCollection(Community::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Api\StoreCommunityRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreCommunityRequest $request)
    {
        return response()->json(Community::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Community  $community
     * @return \App\Http\Resources\Api\CommunityResource
     */
    public function show(Community $community)
    {
        return new CommunityResource($community);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Api\UpdateCommunityRequest  $request
     * @param  \App\Models\Community  $community
     * @return CommunityResource
     */
    public function update(UpdateCommunityRequest $request, Community $community)
    {
        return new CommunityResource($community->update($request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Community  $community
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Community $community)
    {
        return response()->json($community->delete());
    }
}

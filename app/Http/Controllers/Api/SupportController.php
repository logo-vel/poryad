<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreSupportRequest;
use App\Http\Requests\Api\UpdateSupportRequest;
use App\Http\Resources\Api\SupportCollection;
use App\Http\Resources\Api\SupportResource;
use App\Models\Support;
use function response;

class SupportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return SupportCollection
     */
    public function index()
    {
        return new SupportCollection(Support::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Api\StoreSupportRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreSupportRequest $request)
    {
        return response()->json(Support::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Support  $support
     * @return \App\Http\Resources\Api\SupportResource
     */
    public function show(Support $support)
    {
        return new SupportResource($support);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Api\UpdateSupportRequest  $request
     * @param  \App\Models\Support  $support
     * @return \App\Http\Resources\Api\SupportResource
     */
    public function update(UpdateSupportRequest $request, Support $support)
    {
        return new SupportResource($support->update($request->all()));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Support  $support
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Support $support)
    {

        return response()->json($support->delete());
    }
}

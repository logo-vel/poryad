<?php

namespace App\Http\ViewComposers;

use App\Models\Complaint;
use Illuminate\View\View;

class CommonComposer
{
    public function compose(View $view): void
    {
        $view->with('complaint_status_open', Complaint::STATUS_OPEN);
        $view->with('complaint_status_close', Complaint::STATUS_CLOSE);
    }
}

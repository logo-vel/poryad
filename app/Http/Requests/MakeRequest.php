<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MakeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id' => ['required', 'exists:cities,id'],
            'category_id' => ['required', 'exists:support_categories,id'],
            'title' => ['required'],
            'description' => ['required'],
            'degree' => ['required'],
            'delivery' => ['boolean', 'required'],
            'delivery_type' => ['nullable', Rule::requiredIf(request()->boolean('delivery'))],
            'delivery_address' => ['nullable', Rule::requiredIf(request()->boolean('delivery'))],
            'is_other_needy' => ['boolean', 'required'],
            'name' => ['required'],
            'email' => ['required', 'email'],
            'phone' => ['required'],
            'terms' => ['boolean', 'required', Rule::in([true])],
            'is_phone_viber' => ['boolean', 'required'],
            'telegram' => ['nullable'],
            'viber' => ['nullable']
        ];
    }

    public function messages()
    {
        return [
            'delivery_address.required' => "Поле адреса доставки або відділення пошти є обов'язковим для заповнення.",
        ];
    }
}

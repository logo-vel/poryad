<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateSupportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_id' => ['required', 'exists:cities,id'],
            'category_id' => ['required', 'exists:support_categories,id'],
            'id' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'viber' => 'nullable',
            'telegram' => 'nullable',
            'title' => 'required',
            'description' => 'required',
        ];
    }
}

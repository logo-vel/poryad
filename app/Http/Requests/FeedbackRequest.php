<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FeedbackRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:100',
            'email' => 'required|email|max:100',
            'message' => 'required|max:500',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => "Поле Iм'я є обов'язковим для заповнення.",
            'email.required' => "Поле E-mail є обов'язковим для заповнення.",
            'message.required' => "Поле Ваше повідомлення є обов'язковим для заповнення.",
        ];
    }
}

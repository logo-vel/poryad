<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreSupportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status_id' => ['integer', 'required'],
            'city_id' => ['integer', 'required'],
            'category_id' => ['integer', 'required'],
            'status' => ['boolean', 'required'],
            'title' => ['required'],
            'description' => ['required'],
            'degree' => ['required'],
            'delivery' => ['boolean', 'required'],
            'delivery_type' => ['nullable', Rule::requiredIf($this->boolean('delivery'))],
            'delivery_address' => ['nullable', Rule::requiredIf($this->boolean('delivery'))],
            'is_other_needy' => ['boolean', 'required'],
            'name' => ['nullable', Rule::requiredIf($this->boolean('is_other_needy'))],
            'lastname' => ['nullable', Rule::requiredIf($this->boolean('is_other_needy'))],
            'telegram' => ['nullable', Rule::requiredIf($this->boolean('is_other_needy'))],
            'viber' => ['nullable', Rule::requiredIf($this->boolean('is_other_needy'))]
        ];
    }
}

<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSupportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status_id' => ['integer', 'required'],
            'city_id' => ['integer', 'required'],
            'category_id' => ['integer', 'required'],
            'status' => ['boolean', 'required'],
            'title' => ['required'],
            'description' => ['required'],
            'degree' => ['boolean', 'required'],
            'delivery' => ['required'],
            'delivery_type' => ['nullable', 'required'],
            'delivery_address' => ['nullable', 'required'],
            'is_other_needy' => ['boolean', 'required'],
            'name' => ['nullable', 'required'],
            'lastname' => ['nullable', 'required'],
            'telegram' => ['nullable', 'required'],
            'viber' => ['nullable', 'required']
        ];
    }
}

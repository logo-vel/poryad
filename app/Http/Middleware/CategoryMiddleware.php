<?php

namespace App\Http\Middleware;

use App\Http\Resources\ShareSupportCategoryResource;
use Closure;
use Illuminate\Http\Request;
use Inertia\Inertia;

class CategoryMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!app()->runningInConsole()) {
            $categories = \App\Models\SupportCategory::whereStatus(true)->orderBy('sort_order', 'ASC')->get();
            Inertia::share('categories', ShareSupportCategoryResource::collection($categories));
        }
        return $next($request);
    }
}

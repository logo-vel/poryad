<?php

namespace App\Http\Middleware;

use App\Http\Resources\CityHeaderResource;
use Closure;
use Illuminate\Http\Request;
use Inertia\Inertia;

class City
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (!app()->runningInConsole()) {
            $cities = \App\Models\City::whereCategory('misto')->whereStatus(true)->orderBy('sort_order', 'ASC')->get();
            Inertia::share('cities', CityHeaderResource::collection($cities));
        }
        return $next($request);
    }
}

<?php

namespace App\Repositories;

use App\Interfaces\SupportRepositoryInterface;
use App\Models\Support;
use Illuminate\Http\Request;

class SupportRepository implements SupportRepositoryInterface
{
    public function getAll(): \Illuminate\Database\Eloquent\Collection
    {
        return Support::all();
    }

    public function getById(int $id): Support
    {
        return Support::find($id);
    }

    public function getByLimit(int $limit)
    {
        return Support::limit($limit)->whereStatus(true)->latest()->get();
    }

    public function getByCity(int $id): Support
    {
        return Support::whereCityId($id)->whereStatus(true)->latest()->first();
    }

    public function getPaginateByCity(int $id, int $per_page = 12): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return Support::whereCityId($id)->whereStatus(true)->latest()->paginate($per_page);
    }

    public function getByCategory(int $id): Support
    {
        return Support::whereCategoryId($id)->whereStatus(true)->latest()->first();
    }

    public function getPaginateByCategory(int $id, int $per_page = 12): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return Support::whereCategoryId($id)->whereStatus(true)->latest()->paginate($per_page);
    }

    public function getByCityAndCategory(int $city_id, int $category_id): Support
    {
        return Support::whereCityId($city_id)->whereStatus(true)->latest()->whereCategoryId($category_id)->first();
    }

    public function getPaginateByCityAndCategory(int $city_id, int $category_id, int $per_page = 12): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return Support::whereCityId($city_id)->whereStatus(true)->latest()->whereCategoryId($category_id)->paginate($per_page);
    }

    public function getPaginateByUser(int $id, int $per_page = 12): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return Support::whereNeedyUserId($id)->whereStatus(true)->latest()->paginate($per_page);
    }

    protected function filter($query, Request $request)
    {
        switch ($request->get('date')){
            case 'new':
                $query->orderBy('created_at', 'desc');
                break;
            case 'old':
                $query->orderBy('created_at', 'asc');
                break;
            default;
                $query->latest();
                break;
        }

        if($request->has('degree')){
            $data = $request->validate([
                'degree' => 'required'
            ]);
            if ($data['degree'] != 'all') {
                $query->where('degree', $data['degree']);
            }
        }
    }

    public function getByAllFilter(Request $request, int $per_page = 10): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        $query = Support::query();
        $this->filter($query, $request);
        return $query->whereStatus(true)->latest()->paginate($per_page)->withQueryString();
    }

    public function getDegreeByAllFilter(Request $request): array
    {
        $query = Support::query();
        $this->filter($query, $request);
        $items = array_values(array_unique($query->whereStatus(true)->pluck('degree')->toArray()));
        return $items;
    }

    public function getPaginateByCityFilter(Request $request, int $id, int $per_page = 10): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        $query = Support::query();
        $this->filter($query, $request);
        return $query->whereCityId($id)->whereStatus(true)->paginate($per_page)->withQueryString();
    }

    public function getDegreeByCityFilter(Request $request, int $id)
    {
        $query = Support::query();
        $this->filter($query, $request);
        $items = array_values(array_unique($query->whereCityId($id)->whereStatus(true)->pluck('degree')->toArray()));
        return $items;
    }

    public function getPaginateByCategoryFilter(Request $request, int $id, int $per_page = 10): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        $query = Support::query();
        $this->filter($query, $request);
        return $query->whereCategoryId($id)->whereStatus(true)->paginate($per_page)->withQueryString();
    }

    public function getDegreeByCategoryFilter(Request $request, int $id)
    {
        $query = Support::query();
        $this->filter($query, $request);
        $items = array_values(array_unique($query->whereCategoryId($id)->whereStatus(true)->pluck('degree')->toArray()));
        return $items;
    }

    public function getPaginateByCityAndCategoryFilter(Request $request, int $city_id, int $category_id, int $per_page = 10): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        $query = Support::query();
        $this->filter($query, $request);
        return $query->whereCityId($city_id)->whereCategoryId($category_id)->whereStatus(true)->paginate($per_page)->withQueryString();
    }
}

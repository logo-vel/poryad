<?php
namespace App\Helpers\Admin;

use App\Models\Support;

class SupportAPHelper
{
    public static function getIndexVars()
    {
        $listing = Support::where('status', '=', false)->get();
        $items = Support::query()
            ->orderBy('id', 'desc')
            ->with('user', 'supportStatus', 'relation_category', 'relation_city')
            ->paginate(config('system.count_items_in_page'));

        return [
            'items' => $items,
            'listing' => $listing
        ];
    }

    public static function getShowVars($support)
    {
        $support->load('user', 'supportStatus', 'relation_category', 'relation_city');

        return [
            'support' => $support
        ];
    }
}

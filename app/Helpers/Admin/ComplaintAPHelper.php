<?php
namespace App\Helpers\Admin;

use App\Models\Complaint;

class ComplaintAPHelper
{
    public static function getIndexVars()
    {
        $items = Complaint::query()
            ->orderBy('id', 'desc')
            ->with('support', 'support.user')
            ->paginate(config('system.count_items_in_page'));

        return [
            'items' => $items
        ];
    }
}

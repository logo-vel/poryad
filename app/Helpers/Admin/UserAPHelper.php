<?php
namespace App\Helpers\Admin;

use App\Models\User;

class UserAPHelper
{
    public static function getIndexVars()
    {
        $items = User::query()
            ->orderBy('id', 'desc')
            ->with('relation_city')
            ->paginate(config('system.count_items_in_page'));

        return [
            'items' => $items
        ];
    }

    public static function getEditVars($user)
    {
        return [
            'user' => $user
        ];
    }
}

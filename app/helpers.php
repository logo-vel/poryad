<?php

use Carbon\Carbon;

if (! function_exists('date_time_from_db')) {

    function date_time_from_db($date, $format=null)
    {
        if (!$format) {
            $format = config("system.date_time_format");
        }

        if (!$date) {
            return null;
        }

        if (!is_object($date)) {
            $date = Carbon::createFromTimeString($date, 'UTC');
        }

        return $date->setTimezone(config("system.timezone"))->format($format);
    }
}

if (! function_exists('date_from_db')) {

    function date_from_db($date, $format=null)
    {
        if (!$format) {
            $format = config("system.date_format");
        }

        if (!$date) {
            return null;
        } elseif (DateTime::createFromFormat("Y-m-d", $date)) {
            return Carbon::createFromFormat("Y-m-d", $date)->format($format);
        } else {
            return null;
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

/**
* @property $name
* @property $email
* @property $password
* @property $remember_token
 */
class Admin extends Authenticatable
{
    protected $table = 'admins';
    protected $guard = 'admin';
    protected $guarded = [];

    public static function createObj($name, $email, $password)
    {
        $obj = self::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);
    }
}

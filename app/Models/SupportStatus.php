<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SupportStatus
 *
 * @property int $id
 * @property string $title
 * @property string $color
 * @property bool $is_default
 * @property bool $is_notify
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SupportStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportStatus whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportStatus whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportStatus whereIsNotify($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportStatus whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SupportStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'color',
        'is_default',
        'is_notify'
    ];
}

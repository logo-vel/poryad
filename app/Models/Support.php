<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Support
 *
 * @property int $id
 * @property int $status_id
 * @property int $city_id
 * @property int $category_id
 * @property int|null $assist_user_id
 * @property int|null $needy_user_id
 * @property bool $status
 * @property string $title
 * @property string $description
 * @property string $degree
 * @property bool $delivery
 * @property string|null $delivery_type
 * @property string|null $delivery_address
 * @property bool $is_other_needy
 * @property string|null $name
 * @property string|null $lastname
 * @property string|null $telegram
 * @property string|null $viber
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Support newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Support newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Support query()
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereAssistUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereDegree($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereDelivery($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereDeliveryAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereDeliveryType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereIsOtherNeedy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereLastname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereNeedyUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereTelegram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Support whereViber($value)
 * @mixin \Eloquent
 */
class Support extends Model
{
    use HasFactory;

    protected $fillable = [
        'needy_user_id',
        'status_id',
        'city_id',
        'category_id',
        'status',
        'title',
        'description',
        'degree',
        'delivery',
        'delivery_type',
        'delivery_address',
        'is_other_needy',
        'name',
        'lastname',
        'phone',
        'telegram',
        'viber',
        'email'
    ];

    protected $with = [
        'relation_category',
        'relation_city',
        'user'
    ];

    protected $dates = [
        'created_at'
    ];
    // protected $dateFormat = 'd.m.Y';
    public static function findOrFail($id)
    {
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'needy_user_id');
    }

    public function supportStatus()
    {
        return $this->belongsTo(SupportStatus::class, 'status_id');
    }

    public function relation_category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(SupportCategory::class, 'category_id');
    }

    public function relation_city(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function changeStatus($status)
    {
        $this->update([
            'status' => $status
        ]);
    }

    public function deleteObj()
    {
        $this->delete();
    }
}

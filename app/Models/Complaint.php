<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property $status
 * @property $support_id
 * @property $text
 */
class Complaint extends Model
{
    protected $table = 'complaints';
    protected $guarded = [];

    const STATUS_OPEN = "O";
    const STATUS_CLOSE = "C";

    public function support()
    {
        return $this->belongsTo(Support::class, 'support_id');
    }

    public static function createObj($support_id, $text)
    {
        $obj = self::create([
            'support_id' => $support_id,
            'text' => $text
        ]);

        return $obj;
    }

    public function changeStatus($status)
    {
        $this->update([
            'status' => $status
        ]);
    }

    public function deleteObj()
    {
        $this->delete();
    }
}

<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SupportCategory
 *
 * @property int $id
 * @property int|null $parent_id
 * @property bool $status
 * @property string $title
 * @property string $slug
 * @property int $sort_order
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SupportCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|SupportCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportCategory whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportCategory whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportCategory whereSortOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportCategory whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportCategory whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupportCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SupportCategory extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = [
        'title',
        'description',
        'slug',
        'status',
        'sort_order'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}

const mix = require('laravel-mix')
const webpack = require('webpack')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
// mix.copy('node_modules/admin-lte/plugins/fontawesome-free/css/all.min.css', 'public/css/lte/fontawesome.min.css')
// mix.copy('node_modules/admin-lte/dist/css/adminlte.min.css', 'public/css/lte/adminlte.min.css')
// //
// mix.copy('node_modules/admin-lte/plugins/jquery/jquery.min.js', 'public/js/lte/jquery.min.js')
// mix.copy('node_modules/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js', 'public/js/lte/bootstrap.bundle.min.js')
// mix.copy('node_modules/admin-lte/dist/js/adminlte.min.js', 'public/js/lte/adminlte.min.js')
// mix.copy('node_modules/admin-lte/dist/js/demo.js', 'public/js/lte/demo.js')
mix.js([
    'resources/js/app.js',
    'resources/libs/js/common.js',
], 'public/js')
    .vue()
    // .postCss('resources/css/app.css', 'public/css', [
    //     require('postcss-import'),
    //     require('tailwindcss'),
    // ])
    .sass('resources/scss/main.scss', 'public/css')
    .copy('resources/fonts', 'public/fonts')
    .browserSync('poryad.test')
    // .autoload({
    //     jquery: ['$', 'window.$', 'window.jQuery']
    // })

    .webpackConfig({
        plugins: [
            new webpack.ProvidePlugin({
                '$': 'jquery',
                'jQuery': 'jquery',
                'window.jQuery': 'jquery',
            }),
        ]
    })
    .alias({
        '@': 'resources/js',
    })
    .version()

if (mix.inProduction()) {
    mix.version()
}
